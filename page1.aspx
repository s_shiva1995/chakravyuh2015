﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gallerypage.master" AutoEventWireup="true" CodeFile="page1.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="images">
        <img class="a" style="float:left" src="images/10301504_1475141476073392_1526154857024891610_n.jpg" />
        <img class="b" style="float:right" src="images/150098_119631884856214_425459089_n.jpg" />
        <img class="b" style="float:left" src="images/254607_119632104856192_2077352853_n.jpg" />
        <img class="a" style="float:right" src="images/284163_120068154812587_751130320_n.jpg" />
        <img class="a" style="float:left" src="images/285705_119632984856104_2146643736_n.jpg" />
        <img class="b" style="float:right" src="images/304392_119037241582345_1663512767_n.jpg" />
        <img class="b" style="float:left" src="images/304446_119037214915681_1343299990_n.jpg" />
        <img class="a" style="float:right" src="images/316485_120067611479308_1216967270_n.jpg" />
        <img class="a" style="float:left" src="images/32299_119632061522863_1541373137_n.jpg" />
        <img class="b" style="float:right" src="images/375946_119036914915711_1106285317_n.jpg" />
        <img class="b" style="float:left" src="images/424592_120070418145694_188524189_n.jpg" />
        <img class="a" style="float:right" src="images/426182_120068204812582_1501508683_n.jpg" />
        <img class="a" style="float:left" src="images/46331_119632121522857_333936339_n.jpg" />
        <img class="b" style="float:right" src="images/47260_120069158145820_735908182_n.jpg" />
        <img class="b" style="float:left" src="images/485648_119037001582369_1013631117_n.jpg" />
        <img class="a" style="float:right" src="images/527633_120068981479171_729846256_n.jpg" />
        <img class="a" style="float:left" src="images/536307_118588204960582_1724857640_n.jpg" />
        <img class="b" style="float:right" src="images/536310_118583481627721_976598872_n.jpg" />
        <img class="b" style="float:left" src="images/536367_119632488189487_1923063525_n.jpg" />
        <img class="a" style="float:right" src="images/545347_118634698289266_274204135_n.jpg" />
    </div>
    
    <div class="navigation">
        <a href="page5.aspx" class="next" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px; color:#fff; font-size:3em; float:left" >PREV</a>
        <a href="page2.aspx" class="prev" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px;color:#fff; font-size:3em; float:right">NEXT</a>
    </div>
</asp:Content>

