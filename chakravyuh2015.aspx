﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="chakravyuh2015.aspx.cs" Inherits="chakravyuh2015" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CHAKRAVYUH 2015</title>
    <link type="text/css" rel="stylesheet" href="StyleSheet.css" />
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
    <script type="text/javascript" src="JavaScript.js"></script>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <div class="nav">
        <div class="icon-close" title="Close">
            <img src="http://s3.amazonaws.com/codecademy-content/courses/ltp2/img/uber/close.png"/>
        </div>
        <ul>
            <li>
                <p class="home">Home</p>
            </li>
            <li>
                <p class="event">Events</p>
            </li>
            <li>
                <p class="gallery"><asp:HyperLink ID="hplgallery" runat="server" Text="gallery" NavigateUrl="~/page1.aspx"></asp:HyperLink></p>
            </li>
            <li>
                <p class="about">About</p>
            </li>
            <li>
                <p class="achievements">Achievements</p>
            </li>
            <li>
                <p class="help">Help</p>
            </li>
        </ul>
    </div>
    <div class="icon-menu" title="Menu">
        <img src="image/menu-icon.png" style="height: 40px; width: 40px; border-radius: 100%"/>
        <!--<p style="display: inline; position: fixed; top: 15px; left: 60px">Menu</p>-->
        <!--<i class="fa fa-bars"></i>-->
        <!--<p>Menu</p>-->
    </div>
    <div class="open" style="height: 80px; width: 25px; z-index: 2000; cursor: pointer; position: fixed; right: 0px; background-color: #000000; border-bottom: 3px double #419cef; border-left: 3px double #419cef; border-top: 3px double #419cef">
        <img src="image/reg.png" style="width: inherit; height: inherit"/>
    </div>
    <div class="ok" style="z-index: 3000; position: fixed; right: -180px; height: 85px; width:180px; background-color: #ffffff">
        <img src="image/trophy5.png" style="width: 80px; height: 50px; position: relative; top: 18px; left: 5px"/>
        <asp:HyperLink ID="hplregister" runat="server" Text="REGISTER" CssClass="register" NavigateUrl="~/register.aspx"></asp:HyperLink>
        <div class="close" style="height: 20px; width: 20px;  cursor: pointer; position: relative; top: -54px; left: 0">
            <img src="http://s3.amazonaws.com/codecademy-content/courses/ltp2/img/uber/close.png" style="height: inherit; width: inherit"/>
        </div>
        <!--<img src="image/trophy1.png" style="width: 100px; height: 120px; z-index: 1000; cursor: pointer; position: absolute; border-radius: 100%; border: 10px ridge #5e5f57; background-color: #0026ff;"/>-->
    </div>

    <div class="hover panel">
        <div class="front">
            <div class="pad">
                <img src="image/lessfire1.png" alt="logo front" style="width: 100%; height:400px"/>
            </div>
        </div>
        <div class="back">
            <div class="pad">
                <img src="image/1 (1).jpg" class="image1"/>
                <img src="image/1.jpg" class="image1"/>
                <img src="image/103.jpg" class="image1"/>
                <img src="image/14.jpg" class="image1"/>
                <img src="image/65.jpg" class="image1"/>
                <img src="image/7.jpg" class="image1"/>
                <img src="image/95.jpg" class="image1"/>
                <img src="image/89.jpg" class="image1"/>
                <img src="image/96.jpg" class="image1"/>
                <img src="image/80.jpg" class="image1"/>
                <img src="image/6.jpg" class="image1"/>
            </div>
        </div>
    </div>
    <div class="pop" style="width: 50px; height: 30px; margin: auto; background-color: red">

    </div>
    <div class="rotate">
        <img src="image/1 (1).jpg" class="image"/>
        <img src="image/1.jpg" class="image"/>
        <img src="image/103.jpg" class="image"/>
        <img src="image/14.jpg" class="image"/>
        <img src="image/65.jpg" class="image"/>
        <img src="image/7.jpg" class="image"/>
        <img src="image/95.jpg" class="image"/>
        <img src="image/89.jpg" class="image"/>
        <img src="image/96.jpg" class="image"/>
        <img src="image/80.jpg" class="image"/>
        <img src="image/6.jpg" class="image"/>
    </div>
    <div style="width:100%;height:50px;background-color:#b6ff00">

    </div>
    <div style="display: inline">
        <div style="height: 600px; width: 15%; margin-top: 100px; float: left; display: inline-block">
            <img src="image/ball_bat.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 20%; float: left; cursor: pointer" class="fop2" alt="Cricket" title="Cricket"/>
            <img src="image/football1.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 40%; float: left; cursor: pointer" class="fop2" alt="Football" title="Football"/>
            <img src="image/throwball.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 60%; float: left; cursor: pointer" class="fop2" alt="Throwball" title="Throwball"/>
            <img src="image/vollyball.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 60%; float: left; cursor: pointer" class="fop2" alt="Volleyball" title="Volleyball"/>
            <img src="image/tennis.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 40%; float: left; cursor: pointer" class="fop2" alt="Lawn Tennis" title="Lawn Tennis"/>
            <img src="image/basketball1.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-left: 20%; float: left; cursor: pointer" class="fop2" alt="Basketball" title="Basketball"/>
        </div>
        <div style="height: 600px; width: 59%; margin-left: 5.5%; margin-top: 100px; display: inline-block;/* position: relative; left: 10%*/">
            <div class="rules">

            </div>
            <div class="animate">
                <div style="height: 150px; width: 90%; margin-left: auto; margin-right: auto">
                    <video height="150" width="150" autoplay="autoplay" loop="loop">
                        <source src="image/movi.mp4" type="video/mp4"/>
                        Your browser does not support the video tag.
                    </video>
                    <video height="150" width="150" autoplay="autoplay" loop="loop">
                        <source src="image/movi.mp4" type="video/mp4"/>
                        Your browser does not support the video tag.
                    </video>
                    <video height="150" width="150" autoplay="autoplay" loop="loop">
                        <source src="image/movi.mp4" type="video/mp4"/>
                        Your browser does not support the video tag.
                    </video>
                    <video height="150" width="150" autoplay="autoplay" loop="loop">
                        <source src="image/movi.mp4" type="video/mp4"/>
                        Your browser does not support the video tag.
                    </video>
                </div>
            </div>
        </div>
        <div style="height: 600px; width: 15%; margin-top: 100px; float: right; display: inline-block/*; position: relative; top: -700px*/">
            <img src="image/badminton1.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 20%; float: right; cursor: pointer" class="fop2" alt="Badminton" title="Badminton"/>
            <img src="image/teble.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 40%; float: right; cursor: pointer" class="fop2" alt="Table Tennis" title="Table Tennis"/>
            <img src="image/100mrace.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 60%; float: right; cursor: pointer" class="fop2" alt="100mtr Race" title="100mtr Race"/>
            <img src="image/chess.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 60%; float: right; cursor: pointer" class="fop2" alt="Chess" title="Chess"/>
            <img src="image/carrom.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 40%; float: right; cursor: pointer" class="fop2" alt="Carrom" title="Carrom"/>
            <img src="image/weightlift.png" style="width: 100px; height: 70px; margin-bottom: 30px; margin-right: 20%; float: right; cursor: pointer" class="fop2" alt="Weight lifting" title="Weight Lifting"/>
        </div>
    </div>
    <div class="about2">
        <div class="go-right">
            <p style="font-size: xx-large; padding-left: 20px; margin-top: 5px; font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif; text-shadow: 0 0 20px #ffffff; color: #b6ff00"><b>About Us</b></p>
            <img src="image/cricket.JPG" style="height: 300px; width: 95%; box-shadow: 0 0 15px #419cef; border-radius: 10px"/>
        </div>
        <!--<div class="go-left">-->
            <!--<p style="font-size: 22px; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; text-align: justify; padding-right: 15px; color: #5ed216; text-shadow: 0 0 10px #fff">IMS Engineering College, Ghaziabad is a NAAC-accredited and ISO 9001:2008 certified institute which has been rated as No.1 amongst all Private Engineering Colleges in Uttar Pradesh and No.3 amongst all Technical Institutions in Uttar Pradesh including IIT-Kanpur, in a National Survey conducted by CSR-GHRDC. The overall position on all India basis is 23rd amongst Engineering Colleges of Excellence and 33rd amongst all colleges in India. The college has a campus of its own, spread over 10.45 ares of land on the National Highway-24. The college is progressing with the construction of the buildings of the academic sector. The college is having in all 5 hostels, four for boys & one for girls students, accommodating 1800 students. The college has two Canteen, a Students Store, a Dispensary, a Guest House, and play ground for the major games, viz. Football, Basketball, Volleyball and Cricket.</p>-->
        <!--</div>-->
        <br/>
        <br/>
        <br />
        <p style="font-size: 22px; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; text-align: justify; padding-right: 15px; color: #5ed216; text-shadow: 0 0 10px #fff">IMS Engineering College, Ghaziabad is a NAAC-accredited and ISO 9001:2008 certified institute which has been rated as No.1 amongst all Private Engineering Colleges in Uttar Pradesh and No.3 amongst all Technical Institutions in Uttar Pradesh including IIT-Kanpur, in a National Survey conducted by CSR-GHRDC. The overall position on all India basis is 23rd amongst Engineering Colleges of Excellence and 33rd amongst all colleges in India. The college has a campus of its own, spread over 10.45 ares of land on the National Highway-24. The college is progressing with the construction of the buildings of the academic sector. The college is having in all 5 hostels, four for boys & one for girls students, accommodating 1800 students. The college has two Canteen, a Students Store, a Dispensary, a Guest House, and play ground for the major games, viz. Football, Basketball, Volleyball and Cricket.</p>
        <p style="font-size: 22px; font-family: 'Lucida Sans', 'Lucida Sans Regular', 'Lucida Grande', 'Lucida Sans Unicode', Geneva, Verdana, sans-serif; text-align: justify; padding-right: 15px; color: #5ed216; text-shadow: 0 0 10px #fff">About chakjhvk  kuhg khk h k hkh khk gkghgjhgfj hgj hgjgjhg jh gjh gjh gjh gjh gjh gjhgjh gjh gjhg jhg hj gjh gjh ghj gjh gjh gjhgjh gjhgjhgjhg jhg jhgjhgjhghjgjhghj gj</p>
    </div>
    <div class="achieve">

    </div>
    <div class="help2">

    </div>
    <div class="footer">
        <p style="position: relative; bottom: -15px; left: 10px; color: #419cef; text-shadow: 0 0 10px #808080"><b>Copyright © Chakravyuh | All Right Reserved.</b></p>
    </div>
   <!-- <div id="rotating-item-wrapper">
        <img src="image/poop1.jpg" class="rotating-item" />
        <img src="image/poop3.jpg" class="rotating-item" />
        <img src="image/poop2.jpg" class="rotating-item" />
    </div> -->
   </body>
</html>
