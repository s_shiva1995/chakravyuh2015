﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gallerypage.master" AutoEventWireup="true" CodeFile="page4.aspx.cs" Inherits="Default4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="images">
        <img class="a" style="float:left" src="image/62.jpg" />
        <img class="b" style="float:right" src="image/64.jpg" />
        <img class="b" style="float:left" src="image/65.jpg" />
        <img class="a" style="float:right" src="image/66.jpg" />
        <img class="a" style="float:left" src="image/72.jpg" />
        <img class="b" style="float:right" src="image/73.jpg" />
        <img class="b" style="float:left" src="image/77.jpg" />
        <img class="a" style="float:right" src="image/8%20(1).jpg" />
        <img class="a" style="float:left" src="image/80.jpg" />
        <img class="b" style="float:right" src="image/86.jpg" />
        <img class="b" style="float:left" src="image/87.jpg" />
        <img class="a" style="float:right" src="image/89.jpg" />
        <img class="a" style="float:left" src="image/88.jpg" />
        <img class="b" style="float:right" src="image/95.jpg" />
        <img class="b" style="float:left" src="image/96.jpg" />
        <img class="a" style="float:right" src="image/99.jpg" />
        <img class="a" style="float:left" src="image/badminton.jpg" />
        <img class="b" style="float:right" src="image/basketball.jpg" />
        <img class="b" style="float:left" src="image/chess.jpg" />
        <img class="a" style="float:right" src="image/cricket.jpg" />
    </div>
    
    <div class="navigation">
        <a href="page3.aspx" class="next" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px; color:#fff; font-size:3em; float:left" >PREV</a>
        <a href="page5.aspx" class="prev" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px;color:#fff; font-size:3em; float:right">NEXT</a>
    </div>
</asp:Content>

