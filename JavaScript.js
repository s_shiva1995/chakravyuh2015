﻿var main = function () {
    var p;
    $('.hover').hover(function () {
        p = $(this);
        $(p).addClass('flip');
    }, function () {
        $(this).removeClass('flip');
    });
    $('.animate div video').hover(function () {
        $(this).addClass('new-view');
    }, function () {
        $(this).removeClass('new-view');
    });
    $('.image').click(function () {
        $(p).addClass('flip');
    });
    $('.pop').click(function () {
        $(p).removeClass('flip');
    });
    $('.icon-menu').click(function () {
        $('.nav').animate({ left: '0px' }, 200);
    });
    $('.icon-close').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
    });
    $('.home').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
        $("html, body").animate({ scrollTop: "0" });
    });
    $('.event').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
        $("html, body").animate({ scrollTop: "640px" });
    });
    $('.open').click(function () {
        $('.ok').animate({ right: '0' }, 200);
    });
    $('.close').click(function () {
        $('.ok').animate({ right: '-180px' }, 200);
    });
    $('.about').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
        $("html, body").animate({ scrollTop: "1265px" });
    });
    $('.fop2').hover(function () {
        $(this).addClass('new-view');
    }, function(){
        $(this).removeClass('new-view');
    });
    $('.achievements').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
        $("html, body").animate({ scrollTop: "1845px" });
    });
    $('.help').click(function () {
        $('.nav').animate({ left: '-50%' }, 200);
        $("html, body").animate({ scrollTop: "2505px" });
    });
    /*$('.rotate').hover(function () {
        $('.image').click(function () {
            $('.image').removeClass('select');
            $(this).addClass('select');
            for (var i = 0; i < 12; i++)
            {
                if ($('.image').eq(i).hasClass('select'))
                    $('.image1').eq(i).fadeIn('fast');
            }
            //$('.image1').eq(currentItem).fadeOut('fast');
            //var event = $(event.target).attr('class');
            //$('.image1').fadeIn('fast');
        });
    });*/
    /*$('.rotate').hover(function () {
        $('.image').click(function () {
            $('.image').removeClass('select');
            $(this).addClass('select');
        });
    });*/
}
$(document).ready(main);
$(window).load(function () {

    //var InfiniteRotator =
	//{
    init = function (newImage) {
        //alert("p");
	        var initialFadeIn = 1000;
	        var itemInterval = 5000;
	        var fadeTime = 2500;
	        var numberOfItems = $('.image1').length;
	        var currentItem = newImage;
	        //alert(currentItem);
	        $('.image').removeClass('select');
	        $('.image1').eq(currentItem).fadeIn(initialFadeIn);
	        $('.image').eq(currentItem).addClass('select');
	        //var i = 0;
	        $('.image').click(function () {
	            clearInterval(infiniteLoop);
	            return;
	        });
	        infiniteLoop = setInterval(function () {
	            //i++;
	            //alert(i);
	            $('.image1').eq(currentItem).fadeOut(fadeTime);
	            if (currentItem == numberOfItems - 1) {
	                currentItem = 0;
	            } else {
	                currentItem++;
	            }
	            $('.image1').eq(currentItem).fadeIn(fadeTime);
	            $('.image').removeClass('select');
	            $('.image').eq(currentItem).addClass('select');
	        }, itemInterval);
	    }
    //};
    var myTime;
    $('.image').click(function () {
        //delete InfiniteRotator;
        
        //alert(myTime);
        clearInterval(infiniteLoop);
        clearTimeout(myTime);
        var oldImage = pop();
        $('.image').removeClass('select');
        $('.image1').eq(oldImage).fadeOut('fast');
        $(this).addClass('select');
        var newImage = pop();
        $('.image1').eq(newImage).fadeIn('slow');
        myTime = setTimeout(function () {
            $('.image1').eq(newImage).fadeOut('fast');
            if (newImage >= 10)
                init(0);
            else
            /*InfiniteRotator.*/init(newImage + 1);
        }, 5000);
    });
    var pop = function () {
        var i = 0;
        for (i; i < 11; i++) {
            if ($('.image').eq(i).hasClass('select'))
                return i;
        }
    }
    /*$('.rotate').hover(function () {
        $('.image').click(function () {
            $('.image').removeClass('select');
            $(this).addClass('select');
            //$('.image1').fadeOut('fast');
            var i = pop();
            $('.image1').fadeOut(.1);
            $('.image1').eq(i).fadeIn('slow');
            //InfiniteRotator.init();
            //$('.image1').eq(currentItem).fadeOut('fast');
            //var event = $(event.target).attr('class');
            //$('.image1').fadeIn('fast');
        });
    });*/
    /*InfiniteRotator.*/init(0);

});