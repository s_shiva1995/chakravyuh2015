﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link type="text/css" rel="stylesheet" href="StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="head">
        <asp:Label ID="lbl1" runat="server" Text="Chakravyuh 2015" CssClass="peek" Font-Bold="true"></asp:Label>
        <br />
        <br />
        <br />
        <asp:label ID="lbl2" runat="server" Text="Register" CssClass="peek"></asp:label>
    </div> 
    <div class="entry">
        <asp:TextBox ID="txtclgname" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender runat="server" TargetControlID="txtclgname" WatermarkText="Enter your College Name"></asp:TextBoxWatermarkExtender>
        <asp:TextBox ID="txtfaculty" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="water2" runat="server" TargetControlID="txtfaculty" WatermarkText="Enter your Faculty Coordinator Name"></asp:TextBoxWatermarkExtender>
        <asp:TextBox ID="txtmobile" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="wao3" runat="server" TargetControlID="txtmobile" WatermarkText="Enter your Mobile Number"></asp:TextBoxWatermarkExtender>
        <asp:TextBox ID="txtemail" runat="server" CssClass="txtbox"></asp:TextBox>
        <asp:TextBoxWatermarkExtender ID="apo" runat="server" TargetControlID="txtemail" WatermarkText="Enter your Email ID"></asp:TextBoxWatermarkExtender>
    </div>
    <asp:ScriptManager ID="SCR1" runat="server"></asp:ScriptManager>
    </form>
</body>
</html>
