﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gallerypage.master" AutoEventWireup="true" CodeFile="page2.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="images">
        <img class="a" style="float:left" src="images/545554_119037268249009_1406993432_n.jpg" />
        <img class="b" style="float:right" src="images/545555_119631901522879_999574350_n.jpg" />
        <img class="b" style="float:left" src="images/548611_119631754856227_2144471882_n.jpg" />
        <img class="a" style="float:right" src="images/548756_120069128145823_432460452_n.jpg" />
        <img class="a" style="float:left" src="images/560569_118588314960571_818237168_n.jpg" />
        <img class="b" style="float:right" src="images/580235_119636184855784_1748512221_n.jpg" />
        <img class="b" style="float:left" src="images/58752_120066661479403_306886454_n.jpg" />
        <img class="a" style="float:right" src="images/60248_118595408293195_1652579474_n.jpg" />
        <img class="a" style="float:left" src="images/65333_119039794915423_805924579_n.jpg" />
        <img class="b" style="float:right" src="images/65521_119038978248838_1452857095_n.jpg" />
        <img class="b" style="float:left" src="images/66303_120066944812708_1120556933_n.jpg"/>
        <img class="a" style="float:right" src="images/76808_119037894915613_1628448070_n.jpg" />
        <img class="a" style="float:left" src="image/10.jpg" />
        <img class="b" style="float:right" src="image/100.jpg" />
        <img class="b" style="float:left" src="image/101.jpg" />
        <img class="a" style="float:right" src="image/13.jpg" />
        <img class="a" style="float:left" src="image/16.jpg" />
        <img class="b" style="float:right" src="image/17.jpg" />
        <img class="b" style="float:left" src="image/18.jpg" />
        <img class="a" style="float:right" src="image/19.jpg" />
    </div>
    
    <div class="navigation">
        <a href="page1.aspx" class="next" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px; color:#fff; font-size:3em; float:left" >PREV</a>
        <a href="page3.aspx" class="prev" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px;color:#fff; font-size:3em; float:right">NEXT</a>
    </div>
</asp:Content>

