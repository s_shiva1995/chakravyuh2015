﻿<%@ Page Title="" Language="C#" MasterPageFile="~/gallerypage.master" AutoEventWireup="true" CodeFile="page3.aspx.cs" Inherits="Default3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="images">
        <img class="a" style="float:left" src="image/21.jpg" />
        <img class="b" style="float:right" src="image/22.jpg" />
        <img class="b" style="float:left" src="image/23.jpg" />
        <img class="a" style="float:right" src="image/25.jpg" />
        <img class="a" style="float:left" src="image/27.jpg" />
        <img class="b" style="float:right" src="image/3.jpg" />
        <img class="b" style="float:left" src="image/30.jpg" />
        <img class="a" style="float:right" src="image/33.jpg" />
        <img class="a" style="float:left" src="image/4%20(1).jpg" />
        <img class="b" style="float:right" src="image/4.jpg" />
        <img class="b" style="float:left" src="image/40.jpg" />
        <img class="a" style="float:right" src="image/44.jpg" />
        <img class="a" style="float:left" src="image/45.jpg" />
        <img class="b" style="float:right" src="image/5.jpg" />
        <img class="b" style="float:left" src="image/50.jpg" />
        <img class="a" style="float:right" src="image/53.jpg" />
        <img class="a" style="float:left" src="image/57.jpg" />
        <img class="b" style="float:right" src="image/58.jpg" />
        <img class="b" style="float:left" src="image/6%20(1).jpg" />
        <img class="a" style="float:right" src="image/6.jpg" />
    </div>
    
    <div class="navigation">
        <a href="page2.aspx" class="next" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px; color:#fff; font-size:3em; float:left" >PREV</a>
        <a href="page4.aspx" class="prev" style="font-family:'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;margin:20px 20px 20px 30px;color:#fff; font-size:3em; float:right">NEXT</a>
    </div>
</asp:Content>

